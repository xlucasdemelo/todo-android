package com.lucas.lucas.todo;

import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;

import com.lucas.lucas.todo.Entity.Todo;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.EditorAction;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;


@EFragment(R.layout.fragment_form)
@OptionsMenu(value=R.menu.check)
public class FormFragment extends Fragment
{

    /**
     *
     */
    @FragmentArg("todo")
    public Todo todo;

    /**
     *
     */
    @ViewById( R.id.nameEditText )
    public EditText nameEditText;

    /**
     *
     */
    @ViewById( R.id.descricaoEditText )
    public EditText descricaoEditText;

    /**
     *
     */
    @ViewById( R.id.toolbar )
    public Toolbar toolbar;

    @AfterViews
    public void onAfterViews()
    {
        final TodoActivity todoActivity = ( TodoActivity ) this.getActivity();

        this.toolbar = (Toolbar) todoActivity.findViewById( R.id.toolbar );

        todoActivity.setSupportActionBar( this.toolbar );
        todoActivity.getSupportActionBar().setDisplayHomeAsUpEnabled( true );
        todoActivity.setTitle( "teste" );

        if (this.todo != null)
        {
            this.changeToEdit();
        }
    }

    /**
     *
     */
    public void changeToEdit()
    {
        this.nameEditText.setText( this.todo.getName() );
        this.descricaoEditText.setText( this.todo.getDescription() );
    }

    /**
     *
     */
    @OptionsItem(R.id.saveTodo)
    public void saveTodo()
    {
        String name = this.nameEditText.getText().toString();
        String descricao = this.descricaoEditText.getText().toString();

        Todo todo = new Todo(name);
        todo.setDescription(descricao);

        if (this.todo != null)
        {
            ((TodoActivity)getActivity()).todos.remove( this.todo );
        }

        ((TodoActivity)getActivity()).todos.add(todo);

        super.getFragmentManager().popBackStack();//back
    }

    @OptionsItem(R.id.removeTodo)
    public void removeTodo(  )
    {
        ((TodoActivity)getActivity()).todos.remove( this.todo );

        super.getFragmentManager().popBackStack();//back
    }

}
