package com.lucas.lucas.todo;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.lucas.lucas.todo.Entity.Todo;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.SystemService;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

@EFragment(R.layout.fragment_list)
public class ListFragment extends Fragment
{

    @ViewById( R.id.simple_list_item_1 )
    ListView listView;

    // This is the Adapter being used to display the list's data
    private ListAdapter adapter;

    @SystemService
    LayoutInflater inflater;

    @AfterViews
    public void onAfterViews()
    {
        this.buildAdapterListModels();
    }

    @UiThread
    public void buildAdapterListModels()
    {
        // Create an empty adapter we will use to display the loaded data.
        // We pass null for the cursor, then update it in onLoadFinished()
        adapter = listView.getAdapter();

        this.adapter = new ArrayAdapter<Todo>( this.getActivity(), android.R.layout.simple_list_item_1, ((TodoActivity)getActivity()).todos )
        {
            @Override
            public View getView( int position, View convertView, ViewGroup parent )
            {

                ViewHolder viewHolder;

                if( convertView == null )
                {
                    convertView = inflater.inflate( android.R.layout.simple_list_item_1, null );
                    viewHolder = new ViewHolder( convertView );
                    convertView.setTag( viewHolder );
                }
                else
                {
                    viewHolder = ( ViewHolder ) convertView.getTag();
                }

                Todo todo = getItem( position );
                viewHolder.name.setText( todo.getName() ); //default simple_list_item_1

                return convertView;
            }
        };

        this.listView.setAdapter( this.adapter );
    }

    /**
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate( @Nullable Bundle savedInstanceState )
    {
        super.onCreate(savedInstanceState);
    }

    @Click(R.id.fab)
    public void changeToInsert()
    {
        this.openFormFragment(null);
    }

    @Override
    public void onResume() {
        super.onResume();

        final TodoActivity todoActivity = ( TodoActivity ) this.getActivity();

        Toolbar toolbar = (Toolbar) todoActivity.findViewById( R.id.toolbar );

        todoActivity.setSupportActionBar( toolbar );
        todoActivity.getSupportActionBar().setDisplayHomeAsUpEnabled( false );
        todoActivity.setTitle( "teste" );
    }

    /**
     *
     * @param todo
     */
    @ItemClick(R.id.simple_list_item_1)
    public void onTodoItemClick( Todo todo )
    {
        this.openFormFragment( todo );
    }

    public void openFormFragment(Todo todo)
    {
        final FormFragment fragment = FormFragment_.builder()
                .arg( "todo", todo )
                .build();

        this.getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace( R.id.fragmentContainer, fragment )
                .addToBackStack( "name" )
                .commit();
    }

    /**
     *
     */
    public class ViewHolder {
        TextView name;

        public ViewHolder( View view ) {
            name = ( TextView ) view.findViewById( android.R.id.text1 );
        }
    }


}
