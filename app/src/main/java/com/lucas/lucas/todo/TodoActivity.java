package com.lucas.lucas.todo;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.lucas.lucas.todo.Entity.Todo;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

@EActivity( R.layout.activity_todo )
public class TodoActivity extends AppCompatActivity
{

    public List<Todo> todos = new ArrayList<Todo>();

    @ViewById(R.id.toolbar)
    Toolbar toolbar ;

    @AfterViews
    public void onAfterViews()
    {

        this.todos.add(new Todo("Lhavel"));
        this.todos.add(new Todo("Testte"));

        setSupportActionBar(toolbar);
        this.changeToList();
    }

    public void changeToList()
    {
        final ListFragment fragment = ListFragment_.builder()
            .build();

        this.getSupportFragmentManager()
                .beginTransaction()
                .replace( R.id.fragmentContainer, fragment )
                .commit();
    }

    /**
     *
     */
    @OptionsItem(android.R.id.home)
    public void backPressed()
    {
        if (getFragmentManager().getBackStackEntryCount() == 0)
        {
            super.onBackPressed();
        }
        else
        {
            getFragmentManager().popBackStack();
        }
    }

}
